class Node:
    def __init__(self,data):
        self.data = data
        self.next = None
    def insert(self,data):
        current = head
        while current is not None:
            if data == current.data:
                self.next = current.next
                current.next = self
                break
            current = current.next
    def change_head(self,data):
        head.next = head
        head.data = data   
        
head = Node(0)
new_node = Node(1)
new_node.insert(0)
print(head.next.data)

###Time Complexity
import time
length=[8000,16000,24000,32000,40000]
time_=[]
for n in length:
    head = Node(0)
    for i in range(n):
        a = Node(i+1)
        a.insert(i)
    start = time.time()
    b = Node(0)
    b.insert(8000)
    end = time.time()
    print(end-start)
    time_.append(end-start)
print(time_)    

### Time complexity for inserting a node is constant  
### O(n)=1

import matplotlib.pyplot as plt
plt.plot(length,time_)
plt.title('time complexity of inserting node in linked list')
plt.legend()
plt.show()